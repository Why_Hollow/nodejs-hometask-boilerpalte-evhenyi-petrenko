const { fighter } = require('../models/fighter');

function validateFighter(req) {

    Object.keys(fighter)
        .filter((fieldName) => fieldName !== 'id' && fieldName !== 'health')
        .forEach((fieldName) => {
            if (!req.body[fieldName]) {
                throw new Error(`Required field '${fieldName}' is not filled`);
            }
        });

    Object.keys(req.body)

        .forEach((fieldName) => {

            if (!fighter.hasOwnProperty(fieldName)) {

                throw new Error(`not allowed field ${fieldName} `);

            }

        });

    if (req.body.hasOwnProperty('id')) {
        throw new Error('Field \'id\' not allowed for this request'
        );

    }

    if (req.body.power && (typeof req.body.power !== 'number' || req.body.power < 1 || req.body.power > 100)) {
        throw new Error('Power should be a number between 1 and 100');
    }

    if (req.body.defense && (typeof req.body.defense !== 'number' || req.body.defense < 1 || req.body.defense > 10)) {
        throw new Error('Defense should be a number between 1 and 10');
    }
}

const createFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during creation


    try {
        validateFighter(req);

        next();
    } catch (err) {
        res.status(400).send({
            error: true,
            message: `User entity to create isn\'t valid, \n${err}`
        });
    }
}

const updateFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during update
    try {
        validateFighter(req);

        next();
    } catch (err) {
        res.status(400).send({
            error: true,
            message: `Fighter entity to create isn\'t valid, \n${err}`
        });
    }
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;