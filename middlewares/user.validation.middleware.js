const {user} = require('../models/user');

function validateUser(req) {
    Object.keys(user)
        .filter((fieldName) => fieldName !== 'id')
        .forEach((fieldName) => {
            if (!req.body[fieldName]) {
                throw new Error(`Required field '${fieldName}' is not filled`);
            }
        });

    Object.keys(req.body)

        .forEach((fieldName) => {

            if (!user.hasOwnProperty(fieldName)) {

                throw new Error(`not allowed field ${fieldName} `);

            }

        });

    if (req.body.hasOwnProperty('id')) {
        throw new Error('Field \'id\' not allowed for this request'
        );

    }

    if (req.body.email && !/([a-zA-Z0-9]+)([\.{1}])?([a-zA-Z0-9]+)\@gmail([\.])com/gi.test(req.body.email)) {
        throw new Error('Uncorrect email format, allowed only @gmail emails');
    }

    if (req.body.phoneNumber && !/^\+380[0-9]{9}$/g.test(req.body.phoneNumber)) {
        throw new Error('Uncorrect phone number format, should be +380xxxxxxxxx');
    }

    if (req.body.password && req.body.password.length < 3) {
        throw new Error('Password should be min 3 symbols');
    }
}

const createUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during creation

    try {
        validateUser(req);

        next();
    } catch (err) {
        res.status(400).send({
            error: true,
            message: `User entity to create isn\'t valid, \n${err}`
    });
    }
}

const updateUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during update

    try {
        validateUser(req);

        next();
    } catch (err) {
        res.status(400).send({
            error: true,
            message: `User entity to create isn\'t valid, \n${err}`
    });
    }
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;