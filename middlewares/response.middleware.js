const responseMiddleware = (req, res, next) => {
    // TODO: Implement middleware that returns result of the query

    if (res.data) {
        res.send(res.data);
    } else if (res.err) {
        res.status(res.err.status).send({
            error: true,
            message: res.err.message
        });
    }

    next();
}

exports.responseMiddleware = responseMiddleware;