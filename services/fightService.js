const { FightRepository } = require('../repositories/fightRepository');
const {fight} = require('../models/fight');

class FightersService {
    // OPTIONAL TODO: Implement methods to work with fights
	find() {
		const item = FightRepository.getAll();

		if (!item) {
			return [];
		}

		return item;
	}

	search(search) {
		const item = FightRepository.getOne(search);

		if (!item) {
			return null;
		}

		return item;
	}

	create(userData) {
		let filteredUserData = {};

		Object.keys(fight)
			.forEach((fieldName) => {
				if (userData[fieldName]) {
					filteredUserData[fieldName] = userData[fieldName];
				}
			});

		const item = FightRepository.create(filteredUserData);

		if (!item) {
			return null;
		}

		return item;
	}

	update(id, userData) {
		let filteredUserData = {};

		Object.keys(fight)
			.forEach((fieldName) => {
				if (userData[fieldName]) {
					filteredUserData[fieldName] = userData[fieldName];
				}
			});

		FightRepository.update(id, filteredUserData);
	}

	delete(id) {
		FightRepository.delete(id);
	}

}

module.exports = new FightersService();