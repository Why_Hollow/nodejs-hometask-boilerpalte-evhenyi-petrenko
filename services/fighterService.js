const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    // TODO: Implement methods to work with fighters

    find() {
        const item = FighterRepository.getAll();

        if (!item) {
            return [];
        }

        return item;
    }

    findOne(search) {
        const item = FighterRepository.getOne(search);

        if (!item) {
            return null;
        }

        return item;
    }

    create(userData) {
        const item = FighterRepository.create(userData);

        if (!item) {
            return null;
        }

        return item;
    }

    update(id, userData) {
        FighterRepository.update(id, userData);
    }

    delete(id) {
        FighterRepository.delete(id);
    }
}

module.exports = new FighterService();