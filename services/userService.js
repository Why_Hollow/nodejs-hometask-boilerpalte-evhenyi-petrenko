const {UserRepository} = require('../repositories/userRepository');
const {user} = require('../models/user');

class UserService {

    // TODO: Implement methods to work with user
    find() {
        const item = UserRepository.getAll();

        if (!item) {
            return [];
        }

        return item;
    }

    search(search) {
        const item = UserRepository.getOne(search);

        if (!item) {
            return null;
        }

        return item;
    }

    create(userData) {
        let filteredUserData = {};

        Object.keys(user)
            .forEach((fieldName) => {
                if (userData[fieldName]) {
                    filteredUserData[fieldName] = userData[fieldName];
                }
            });

        const item = UserRepository.create(filteredUserData);

        if (!item) {
            return null;
        }

        return item;
    }

    update(id, userData) {
        let filteredUserData = {};

        Object.keys(user)
            .forEach((fieldName) => {
                if (userData[fieldName]) {
                    filteredUserData[fieldName] = userData[fieldName];
                }
            });

        UserRepository.update(id, filteredUserData);
    }

    delete(id) {
        UserRepository.delete(id);
    }
}

module.exports = new UserService();