const { Router } = require('express');
const FightService = require('../services/fightService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');


const router = Router();

router.get('/', (req, res, next) => {
	try {
		const data = FightService.find();

		res.data = data;
	} catch (err) {
		res.err = err;
	} finally {
		next();
	}
}, responseMiddleware);

router.get('/:id', (req, res, next) => {
	try {
		const data = FightService.search({id: req.params.id});

		if (data) {
			res.data = data;
		} else {
			res.err = {
				status: 404,
				message: 'User not found'
			};
		}
	} catch (err) {
		res.err = err;
	} finally {
		next();
	}
}, responseMiddleware);

router.post('/',
	(req, res, next) => {
		try {
			const data = FightService.create(req.body);

			res.data = data;
		} catch (err) {
			res.err = err;
		} finally {
			next();
		}
	}, responseMiddleware);

router.put('/:id',
	(req, res, next) => {
		try {
			const data = FightService.update(req.params.id, req.body);

			res.data = data;
		} catch (err) {
			res.err = err;
		} finally {
			next();
		}
	}, responseMiddleware);

router.delete('/:id', (req, res, next) => {
	try {

		const data = FightService.delete(req.params.id);
		console.log(req.params.id);
		res.data = data;
	} catch (err) {
		res.err = err;
	} finally {
		next();
	}
}, responseMiddleware);

module.exports = router;