const {Router} = require('express');
const FighterService = require('../services/fighterService');
const {responseMiddleware} = require('../middlewares/response.middleware');
const {createFighterValid, updateFighterValid} = require('../middlewares/fighter.validation.middleware');

const router = Router();

// TODO: Implement route controllers for fighter

router.get('/', (req, res, next) => {
    try {
        const data = FighterService.find();

        res.data = data;
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

router.get('/:id', (req, res, next) => {
    try {
        const data = FighterService.findOne({id: req.params.id});

        if (data) {
            res.data = data;
        } else {
            res.err = {
                status: 404,
                message: 'Fighter not found'
            };
        }
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

router.post('/', createFighterValid,
    (req, res, next) => {
        try {
            const data = FighterService.create(req.body);

            res.data = data;
        } catch (err) {
            res.err = err;
        } finally {
            next();
        }
    }, responseMiddleware);

router.put('/:id', updateFighterValid,
    (req, res, next) => {
        try {
            const data = FighterService.update(req.params.id, req.body);

            if (data) {
                res.data = data;
            } else {
                res.err = {
                    status: 404,
                    message: 'Fighter not found'
                };
            }
        } catch (err) {
            res.err = err;
        } finally {
            next();
        }
    }, responseMiddleware);

router.delete('/:id', (req, res, next) => {
    try {
        const data = FighterService.delete(req.params.id);

        res.data = data;
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

module.exports = router;