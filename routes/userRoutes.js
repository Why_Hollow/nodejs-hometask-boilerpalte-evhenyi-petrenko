const {Router} = require('express');
const UserService = require('../services/userService');
const {createUserValid, updateUserValid} = require('../middlewares/user.validation.middleware');
const {responseMiddleware} = require('../middlewares/response.middleware');

const router = Router();

// TODO: Implement route controllers for user

router.get('/', (req, res, next) => {
    try {
        const data = UserService.find();

        res.data = data;
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

router.get('/:id', (req, res, next) => {
    try {
        const data = UserService.search({id: req.params.id});

        if (data) {
            res.data = data;
        } else {
            res.err = {
                status: 404,
                message: 'User not found'
            };
        }
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

router.post('/', createUserValid,
    (req, res, next) => {
        try {
            const data = UserService.create(req.body);

            res.data = data;
        } catch (err) {
            res.err = err;
        } finally {
            next();
        }
    }, responseMiddleware);

router.put('/:id', updateUserValid,
    (req, res, next) => {
        try {
            const data = UserService.update(req.params.id, req.body);

            res.data = data;
        } catch (err) {
            res.err = err;
        } finally {
            next();
        }
    }, responseMiddleware);

router.delete('/:id', (req, res, next) => {
    try {

        const data = UserService.delete(req.params.id);
        console.log(req.params.id);
        res.data = data;
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

module.exports = router;